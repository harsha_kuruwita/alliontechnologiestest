//
//  Home.m
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import "Home.h"
#import "StoreModel.h"
#import "mainCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "StoreView.h"
#import "LLSlideMenu.h"
#import "AddStore.h"
#import "SliderMenu.h"

@interface Home ()
@property (weak, nonatomic) IBOutlet UITableView *storeTableview;
@property (strong, nonatomic) NSMutableArray *storeNameArr;
@property (strong, nonatomic) NSMutableArray *storeSubNameArr;
@property (strong, nonatomic) NSMutableArray *storeImgArr;
@property (strong, nonatomic) NSMutableArray *storeIdArr;

@end

@implementation Home

- (void)viewDidLoad {
    [super viewDidLoad];
    
       self.storeNameArr = [NSMutableArray array];
       self.storeSubNameArr= [NSMutableArray array];
        self.storeImgArr= [NSMutableArray array];
        self.storeIdArr= [NSMutableArray array];
    
    //Remove empty cell
   _storeTableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //Add Humburge button
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 110, 50)];
    view.backgroundColor = [UIColor clearColor];
    
    UIButton *settingsButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [settingsButton setImage:[UIImage imageNamed:@"Fw96Z"] forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(openAdd:) forControlEvents:UIControlEventTouchUpInside];
    [settingsButton setFrame:CGRectMake(80,5,32,32)];
    [view addSubview:settingsButton];
    
   
    
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:view];
    [self getData];
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)getData{
    
    //Call database and reload tableview
    RLMResults<StoreModel *> *storeObject = [StoreModel allObjects];
 
    for (StoreModel *object in storeObject) {
        
        [_storeNameArr addObject:object.storeName];
        [_storeSubNameArr addObject:object.storeSubName];
        [_storeImgArr addObject:object.storeImgUrl];
        [_storeIdArr addObject:object.storeId];
        
        
    }
    _storeTableview.reloadData;
}


//UITable view implementation 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_storeNameArr count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    mainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"mainCell" bundle:nil] forCellReuseIdentifier:@"mainCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    }
   
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell setNeedsDisplay];
        cell.storeNameFild.text = _storeNameArr[indexPath.row];
        cell.storeSubName.text = _storeSubNameArr[indexPath.row];
        
        
        cell.storePic.layer.cornerRadius = cell.storePic.frame.size.width / 2;
        cell.storePic.clipsToBounds = YES;
        cell.storePic.layer.borderWidth = 0.7f;
        cell.storePic.layer.borderColor = [UIColor grayColor].CGColor;
        
        
        
        NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(
                                                                NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDirectory = [sysPaths objectAtIndex:0];
        
        NSString *filePath =
        [NSString stringWithFormat:@"%@/profileimage/imageheader.jpg", docDirectory];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            cell.storePic.image = [[UIImage alloc] initWithContentsOfFile:filePath];
        }else{
            
            NSString *urlString = [NSString stringWithFormat:@"%@", _storeImgArr[indexPath.row]];
            
            [cell.storePic sd_setImageWithURL:[NSURL URLWithString:urlString ]
                               placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        }
        
        
        
        
        
     });
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StoreView *VC = [[StoreView alloc] init];
    VC.hidesBottomBarWhenPushed = YES;
    VC.storeId = _storeIdArr[indexPath.row];
    [self.navigationController pushViewController:VC animated:YES];
   
}

- (IBAction)openAdd:(id)sender
{
    
    SliderMenu *VC = [[SliderMenu alloc] init];
    VC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:VC animated:YES];
    
    
}

@end
