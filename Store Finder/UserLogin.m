//
//  UserLogin.m
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import "UserLogin.h"
#import "UserModel.h"
#import "Home.h"
#import <EHPlainAlert/EHPlainAlert.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface UserLogin ()
@property (weak, nonatomic) IBOutlet UITextField *useremailFild;
@property (weak, nonatomic) IBOutlet UITextField *userpasswordFild;

@end

@implementation UserLogin

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




// user login with user name and password
- (IBAction)naiveLogin:(id)sender
{
    
    NSString *whereQury = [NSString stringWithFormat:@"%@%@%@%@%@", @"email == '", _useremailFild.text, @"' and password =='",_userpasswordFild.text,@"'"];
    
    RLMResults<UserModel *> *userModels = [UserModel objectsWhere:whereQury];
    
    if(userModels.count>0){
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
        UIWindow *window =  [[[UIApplication sharedApplication] windows] firstObject];
        Home *homeVc = [[Home alloc]initWithNibName:@"Home" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
        window.rootViewController = nav;
        [window makeKeyAndVisible];
        
    }else{
       [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Email or password incorect" type:ViewAlertError];
    }

}

- (IBAction)loginWithFb:(id)sender{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    if ([UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:@"fb://"]])
    {
        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    }
    
    [login logInWithReadPermissions:@[@"public_profile", @"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            NSLog(@"Unexpected login error: %@", error);
            NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
            NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
            [[[UIAlertView alloc] initWithTitle:alertTitle
                                        message:alertMessage
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
        else
        {
            if(result.token)   // This means if There is current access token.
            {
                
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                   parameters:@{@"fields": @"picture, name, email"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id userinfo, NSError *error) {
                     if (!error) {
                         
                         
                         dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                         dispatch_async(queue, ^(void) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                NSString *username = [NSString stringWithFormat:@"%@",[userinfo objectForKey:@"name"]];
                                 NSString *email = [NSString stringWithFormat:@"%@",[userinfo objectForKey:@"email"]];
                                 
                                 
                                 //Save data in realm database
                                 UserModel *userModel = [[UserModel alloc] init];
                                 userModel.userid = [[NSUUID UUID] UUIDString];
                                 userModel.email = email;
                                 userModel.name = username;
                                 userModel.password = username;
                                 RLMRealm *realm = [RLMRealm defaultRealm];
                                 [realm beginWriteTransaction];
                                 [realm addObject:userModel];
                                 [realm commitWriteTransaction];
                                 
                                 
                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
                                 UIWindow *window =  [[[UIApplication sharedApplication] windows] firstObject];
                                 Home *homeVc = [[Home alloc]initWithNibName:@"Home" bundle:nil];
                                 UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
                                 window.rootViewController = nav;
                                 [window makeKeyAndVisible];
                                 
                             });
                         });
                         
                     }
                     else{
                         
                         NSLog(@"%@", [error localizedDescription]);
                     }
                 }];
            }
            NSLog(@"Login Cancel");
        }
    }];
}

@end
