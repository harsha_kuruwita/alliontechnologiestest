//
//  StoreModel.m
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import "StoreModel.h"

@implementation StoreModel

+ (NSString *)primaryKey {
    return @"storeId";
}

@end
