//
//  StoreView.m
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import "StoreView.h"
#import "StoreModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <GoogleMaps/GoogleMaps.h>
#import "AddStore.h"

@interface StoreView ()
@property (weak, nonatomic) IBOutlet UIImageView *storeImg;
@property (weak, nonatomic) IBOutlet UILabel *storeNameFild;
@property (weak, nonatomic) IBOutlet UILabel *storeDesLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UIView *googleMapHolder;
@property (weak, nonatomic) IBOutlet GMSMapView *googleMap;
@property (weak, nonatomic) IBOutlet UIView *infobar;
@property (weak, nonatomic) IBOutlet UIView *mapbar;

@end

@implementation StoreView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_googleMapHolder setHidden:YES];
    [_mapbar setHidden:YES];
    
    NSString *whereQury = [NSString stringWithFormat:@"%@%@%@", @"storeId == '", _storeId, @"'"];
    
    RLMResults<StoreModel *> *storeObject = [StoreModel objectsWhere:whereQury];
    NSString *storename;
    NSString *storeDes;
    NSString *storeImgrl;
    NSString *storeEmail;
    NSString *storePhone;
    for (StoreModel *object in storeObject) {
        
        storename = object.storeName;
        storeDes = object.storeSubName;
        storeImgrl = object.storeImgUrl;
        storeEmail = object.storeEmail;
        storePhone = object.storeMobile;
    }
    _storeNameFild.text = storename;
    _storeDesLbl.text = storeDes;
    _emailLbl.text = storeEmail;
    _phoneLbl.text = storePhone;
    
    _storeImg.layer.cornerRadius = _storeImg.frame.size.width / 2;
    _storeImg.clipsToBounds = YES;
    _storeImg.layer.borderWidth = 0.7f;
    _storeImg.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(
                                                            NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    
    NSString *filePath =
    [NSString stringWithFormat:@"%@/profileimage/imageheader.jpg", docDirectory];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        _storeImg.image = [[UIImage alloc] initWithContentsOfFile:filePath];
    }else{
        
        NSString *urlString = [NSString stringWithFormat:@"%@", storeImgrl];
        
        [_storeImg sd_setImageWithURL:[NSURL URLWithString:urlString ]
                         placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
   
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:6.9218374
                                                            longitude:79.8211859
                                                                 zoom:8];
    _googleMap.camera = camera;
    _googleMap.myLocationEnabled = YES;
    [self fetchPolylineWithDestination:@"Maharagama"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)showInfo:(id)sender
{
   [_googleMapHolder setHidden:YES];
    [_mapbar setHidden:YES];
    [_mapbar setHidden:YES];
    [_infobar setHidden:NO];

}

- (IBAction)showMap:(id)sender
{
    [_googleMapHolder setHidden:NO];
    [_mapbar setHidden:NO];
    [_infobar setHidden:YES];
    
    
    
}

- (void)fetchPolylineWithDestination:(NSString *)destinationString
{
    NSString *originString = [NSString stringWithFormat:@"%f,%f", 7.15235, 80.1035];
    NSString *destinationEncodedString = [destinationString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=[API_KEY]", directionsAPI, originString, destinationEncodedString];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error) {
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     
                                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                                         if (error) {
                                                             
                                                             NSLog(@"error: %@", error);
                                                             return;
                                                         }
                                                         
                                                         NSArray *routesArray = [json objectForKey:@"routes"];
                                                         
                                                         GMSPolyline *polyline = nil;
                                                         if ([routesArray count] > 0) {
                                                             NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                             NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                             NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                             GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                             polyline = [GMSPolyline polylineWithPath:path];
                                                         }
                                                         
                                                         if(polyline)
                                                             polyline.map = _googleMap;
                                                        
                                                         
                                                     });
                                                 }];
    [fetchDirectionsTask resume];
}

@end
