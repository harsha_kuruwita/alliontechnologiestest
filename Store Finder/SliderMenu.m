//
//  SliderMenu.m
//  Store Finder
//
//  Created by WAKESYS  on 11/14/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import "SliderMenu.h"
#import "Promotions.h"
#import "AddStore.h"
#import "UserRegistation.h"

@interface SliderMenu ()

@end

@implementation SliderMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Transparent navigation bar
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openPromotions:(id)sender
{
    
    Promotions *VC = [[Promotions alloc] init];
    VC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:VC animated:YES];
    
}

- (IBAction)openAddStore:(id)sender
{
    
    AddStore *VC = [[AddStore alloc] init];
    VC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:VC animated:YES];
    
}

- (IBAction)openLogout:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
    UIWindow *window =  [[[UIApplication sharedApplication] windows] firstObject];
    UserRegistation *logoutvc = [[UserRegistation alloc]initWithNibName:@"UserRegistation" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:logoutvc];
    window.rootViewController = nav;
    [window makeKeyAndVisible];
    
}

@end
