//
//  UserRegistation.m
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import "UserRegistation.h"
#import <EHPlainAlert/EHPlainAlert.h>
#import "UserLogin.h"
#import "UserModel.h"
#import "Home.h"

@interface UserRegistation ()
@property (weak, nonatomic) IBOutlet UITextField *namefild;
@property (weak, nonatomic) IBOutlet UITextField *emailfild;
@property (weak, nonatomic) IBOutlet UITextField *passwordfild;

@end

@implementation UserRegistation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

/**
 * User sign up with name,email and password if user does not exists it will save on realm database
 * @author Harsha
 *
 * @param name Source timeline entity ID
 * @param email Destination timeline entity ID
 * @param password Destination timeline entity ID
 * @return void
 */

- (IBAction)sighnUp:(id)sender {
    
    
    NSString *name =  _namefild.text;
    NSString *email =  _emailfild.text;
    NSString *password =  _passwordfild.text;
    
    //Show error notification
    if([name  isEqual: @""]){
        [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please Enter Your Name" type:ViewAlertError];
        
    }else if([email  isEqual: @""]){
         [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please Enter Your Email" type:ViewAlertError];
        
    }else if([password  isEqual: @""]){
         [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please Enter Your Password" type:ViewAlertError];
    }else if(![self NSStringIsValidEmail:email]){
        [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Invalid Email Adders" type:ViewAlertError];
    }else{
        
        //Realm qury
        NSString *whereQury = [NSString stringWithFormat:@"%@%@%@", @"email == '", email, @"'"];
        
        RLMResults<UserModel *> *userModels = [UserModel objectsWhere:whereQury];
        
        if(userModels.count>0){
             [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Account Already Exists" type:ViewAlertError];
        }else{
            UserModel *userModel = [[UserModel alloc] init];
            userModel.userid = [[NSUUID UUID] UUIDString];
            userModel.email = email;
            userModel.name = name;
            userModel.password = password;
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [realm addObject:userModel];
            [realm commitWriteTransaction];
            //open home as root view
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
            
            
            UIWindow *window =  [[[UIApplication sharedApplication] windows] firstObject];
            Home *homeVc = [[Home alloc]initWithNibName:@"Home" bundle:nil];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
            window.rootViewController = nav;
            [window makeKeyAndVisible];
        }
        
        
       
        
    }

}

/**
 * Already have account user can login
 * @author Harsha
 */

- (IBAction)openSignIn:(id)sender
{
    UserLogin *VC = [[UserLogin alloc] init];
    VC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:VC animated:YES];
}

//Email Validation
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
