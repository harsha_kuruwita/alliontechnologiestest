//
//  Promotions.m
//  Store Finder
//
//  Created by WAKESYS  on 11/14/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import "Promotions.h"
#import "AFNetworking.h"
#import "PromotionCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface Promotions ()
@property (weak, nonatomic) IBOutlet UITableView *promotionTable;
@property (strong, nonatomic) NSMutableArray *spromoNameArr;
@property (strong, nonatomic) NSMutableArray *spromoImgArr;

@end

@implementation Promotions

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //Transparent navigation bar
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    self.spromoNameArr = [NSMutableArray array];
    self.spromoImgArr = [NSMutableArray array];
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getData{
    
    
  NSString *uri = @"https://api.github.com/users";
    
    AFHTTPSessionManager *loginManager = [[AFHTTPSessionManager alloc] init];
    
  
    
    NSDictionary *params = @{};
    
    
    [loginManager GET:uri parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        
        NSLog(@"AppJSONcccvvv: %@", responseObject);
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        
        NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData: jsonData options:NSJSONReadingMutableContainers error: &error];
        
        for (NSArray *myElement in jsonArray) {
           [_spromoNameArr addObject:[myElement valueForKey:@"login"]];
             [_spromoImgArr addObject:[myElement valueForKey:@"avatar_url"]];
        }
        
        _promotionTable.reloadData;
        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            NSMutableArray *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        
    }
               failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                   NSLog(@"%@", error);
               
                   
               }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_spromoNameArr count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 190;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PromotionCell * cell = [tableView dequeueReusableCellWithIdentifier:@"promotionCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"PromotionCell" bundle:nil] forCellReuseIdentifier:@"promotionCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"promotionCell"];
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell setNeedsDisplay];
      
        
        cell.HeadderName.text = _spromoNameArr[indexPath.row];
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect: cell.CellMainView.bounds];
        cell.CellMainView.layer.masksToBounds = NO;
         cell.CellMainView.layer.shadowColor = [UIColor blackColor].CGColor;
         cell.CellMainView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
         cell.CellMainView.layer.shadowOpacity = 0.5f;
         cell.CellMainView.layer.shadowPath = shadowPath.CGPath;
        
        NSString *urlString = [NSString stringWithFormat:@"%@", _spromoImgArr[indexPath.row]];
        
        [cell.headderimg sd_setImageWithURL:[NSURL URLWithString:urlString ]
                         placeholderImage:[UIImage imageNamed:_spromoNameArr[indexPath.row]]];
        
        
        
    });
    
    return cell;
}


@end
