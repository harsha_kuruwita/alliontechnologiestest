//
//  PromotionCell.h
//  Store Finder
//
//  Created by WAKESYS  on 11/14/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *HeadderName;
@property (weak, nonatomic) IBOutlet UIImageView *headderimg;
@property (weak, nonatomic) IBOutlet UIView *CellMainView;

@end
