//
//  AddStore.m
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import "AddStore.h"
#import <EHPlainAlert/EHPlainAlert.h>
#import "StoreModel.h"

@interface AddStore ()
@property (weak, nonatomic) IBOutlet UITextField *storenmaetextFild;
@property (weak, nonatomic) IBOutlet UITextField *storeAddersFild;
@property (weak, nonatomic) IBOutlet UITextField *storeEmailFild;
@property (weak, nonatomic) IBOutlet UITextField *storephoneFild;
@property (weak, nonatomic) IBOutlet UITextField *storeDiscription;
@property (weak, nonatomic) IBOutlet UITextField *storeImageUrl;

@end

@implementation AddStore

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //Transparent navigation bar
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveStore:(id)sender {
    
    
    NSString *stname =  _storenmaetextFild.text;
    NSString *stAdders =  _storeAddersFild.text;
    NSString *stEmail =  _storeEmailFild.text;
     NSString *stphone =  _storephoneFild.text;
    NSString *stdis =  _storeDiscription.text;
    NSString *stImg =  _storeImageUrl.text;
    
    if([stname  isEqual: @""]){
        [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please enter store name" type:ViewAlertError];
        
    }else if([stAdders  isEqual: @""]){
        [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please enter store adders" type:ViewAlertError];
        
    }else if([stEmail  isEqual: @""]){
        [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please enter store email" type:ViewAlertError];
    }else if([stphone  isEqual: @""]){
        [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please enter store mobile" type:ViewAlertError];
    }else if([stdis  isEqual: @""]){
        [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please enter store disription" type:ViewAlertError];
    }else if([stImg  isEqual: @""]){
        [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Please enter store image url" type:ViewAlertError];
    }else{
        
        
        NSString *whereQury = [NSString stringWithFormat:@"%@%@%@", @"storeName == '", stname, @"'"];
        
        RLMResults<StoreModel *> *storeModels = [StoreModel objectsWhere:whereQury];
        
        if(storeModels.count>0){
            [EHPlainAlert showAlertWithTitle:@"Sorry" message:@"Store Already Exist" type:ViewAlertError];
        }else{
            StoreModel *storeModel = [[StoreModel alloc] init];
            storeModel.storeId = [[NSUUID UUID] UUIDString];
            storeModel.storeName = stname;
            storeModel.storeEmail = stEmail;
            storeModel.storeMobile = stphone;
            storeModel.storeSubName = stdis;
            storeModel.storeImgUrl = stImg;
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];

            [realm addObject:storeModel];
            [realm commitWriteTransaction];
        }
        
        
        
        
    }
    
}

@end
