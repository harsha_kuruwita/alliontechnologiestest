//
//  StoreModel.h
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import <Realm/Realm.h>

@interface StoreModel : RLMObject

@property (nonatomic, strong) NSString *storeId;
@property (nonatomic, strong) NSString *storeName;
@property (nonatomic, strong) NSString *storeSubName;
@property (nonatomic, strong) NSString *storeImgUrl;
@property (nonatomic, strong) NSString *storelat;
@property (nonatomic, strong) NSString *storelong;
@property (nonatomic, strong) NSString *storeMobile;
@property (nonatomic, strong) NSString *storeEmail;

@end
