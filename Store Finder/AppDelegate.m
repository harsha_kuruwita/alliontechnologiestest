//
//  AppDelegate.m
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

@import GoogleMaps;
@import GooglePlaces;

#import "AppDelegate.h"
#import "UserRegistation.h"
#import "UserModel.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Home.h"
#import "StoreModel.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
   NSLog(@"Relam path ::::%@",[RLMRealmConfiguration defaultConfiguration].fileURL);//DB PATH
    
    [GMSServices provideAPIKey:@"AIzaSyDVmPNYYogUrHLQbkXAAoecYvwJ0TFS3Vo"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyDVmPNYYogUrHLQbkXAAoecYvwJ0TFS3Vo"];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"]) {
        self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        Home *homeVc = [[Home alloc]initWithNibName:@"Home" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
        self.window.rootViewController = nav;
        [self.window makeKeyAndVisible];
        return YES;
        
    }else{
        // add sample store data in database for fist open
        StoreModel *storeModel = [[StoreModel alloc] init];
        storeModel.storeId = [[NSUUID UUID] UUIDString];
        storeModel.storeName = @"Colombo Store";
        storeModel.storeSubName = @"Famus juwelary Store";
        storeModel.storeImgUrl = @"https://pbs.twimg.com/profile_images/1511714284/dstore.jpg";
        storeModel.storelat = @"6.93297";
        storeModel.storelong = @"79.84243";
        storeModel.storeMobile = @"0774485621";
        storeModel.storeEmail = @"store@mail.com";
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm addObject:storeModel];
        [realm commitWriteTransaction];
        
        self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        UserRegistation *homeVc = [[UserRegistation alloc]initWithNibName:@"UserRegistation" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
        self.window.rootViewController = nav;
        [self.window makeKeyAndVisible];
        return YES;
        
        
    }
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
