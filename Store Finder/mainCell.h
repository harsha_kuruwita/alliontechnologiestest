//
//  mainCell.h
//  Store Finder
//
//  Created by WAKESYS  on 11/13/17.
//  Copyright © 2017 AllionTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mainCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *storeNameFild;
@property (weak, nonatomic) IBOutlet UILabel *storeSubName;
@property (weak, nonatomic) IBOutlet UIImageView *storePic;

@end
